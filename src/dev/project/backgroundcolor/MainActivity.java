package dev.project.backgroundcolor;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button button = (Button) findViewById(R.id.buttonChangeBgColor);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ChangeBackgroundColor();
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	int currentColor = R.color.green;
	void ChangeBackgroundColor() {

		View viewMain = findViewById(R.id.layoutViewMain);
		// int color = ((ColorDrawable) viewMain.getBackground()).getColor();

		if (currentColor == R.color.green) {
			viewMain.setBackgroundColor(getResources().getColor(R.color.yellow));
			currentColor = R.color.yellow;
		} else if(currentColor == R.color.yellow){
			viewMain.setBackgroundColor(getResources().getColor(R.color.red));
			currentColor = R.color.red;
		} else if(currentColor == R.color.red){
			viewMain.setBackgroundColor(getResources().getColor(R.color.green));
			currentColor = R.color.green;
		}
	}
}
